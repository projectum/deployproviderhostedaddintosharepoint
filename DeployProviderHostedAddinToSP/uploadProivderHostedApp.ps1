[CmdletBinding()]
param()

Trace-VstsEnteringInvocation $MyInvocation
try {	
	$paramFile = Get-VstsInput -Name appFile -Require

	# get SharePoint add-in tokens
	$paramTokenAddinTitle = Get-VstsInput -Name tokenAddinTitle
	$paramTokenAddinFileName = Get-VstsInput -Name tokenAddinFileName
	$paramTokenClientID = Get-VstsInput -Name tokenClientId
	$paramTokenAzureBackendUrl = Get-VstsInput -Name tokenAzureBackendUrl
	$paramTokenProductId = Get-VstsInput -Name tokenProductId

    # get app catalog connection details
    $paramAppCatalogUrl = Get-VstsInput -Name appCatalogUrl -Require
	$paramAppCatalogLogin = Get-VstsInput -Name appCatalogLogin -Require
	$paramAppCatalogPassword = Get-VstsInput -Name appCatalogPassword -Require	

	# log properties to task output
	Write-Host "Add-in title: $paramTokenAddinTitle"
	Write-Host "Add-in file name: $paramTokenAddinFileName"
	Write-Host "Azure backend URI: $paramTokenAzureBackendUrl"
	Write-Host "Product ID: $paramTokenProductId"
    Write-Host "App catalog URL: $paramAppCatalogUrl"
	Write-Host "App catalog login: $paramAppCatalogLogin"

	$paramTokenAzureBackendUrl = $paramTokenAzureBackendUrl.TrimEnd('/')

	[Guid]$productId = [Guid]::Empty;

	if ($paramTokenProductId -ne "" -and ![Guid]::TryParse($paramTokenProductId , [ref]$productId)) {
		Throw New-Object System.FormatException "Invalid Product ID format."
	}

	# load required dll's
	[System.Reflection.Assembly]::LoadWithPartialName("System.Net.Mime") | Out-Null
	[System.Reflection.Assembly]::LoadWithPartialName("System.Xml.Linq") | Out-Null
	[System.Reflection.Assembly]::Load("WindowsBase,Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35") | Out-Null

	# load SharePoint CSOM assemblies
	Add-Type -Path Microsoft.SharePoint.Client.dll
	Add-Type -Path Microsoft.SharePoint.Client.Runtime.dll
	Add-Type -Path Microsoft.SharePoint.Client.UserProfiles.dll

	# functions
	function ProcessAppManifest($zipPackage) {
		$part = GetPart $zipPackage '/appManifest.xml'
		$partSream = $part.GetStream()

		$xDoc = [System.Xml.Linq.XDocument]::Load($partSream)
		$nsManager = New-Object System.Xml.XmlNamespaceManager(New-Object System.Xml.NameTable)
		$nsManager.AddNamespace("ns", "http://schemas.microsoft.com/sharepoint/2012/app/manifest")

		if ($paramTokenAddinTitle -ne '') {
			$elTitle = [System.Xml.XPath.Extensions]::XPathSelectElement($xDoc, "//ns:App/ns:Properties/ns:Title", $nsManager)
			$elTitle.Value = $paramTokenAddinTitle
		}

		if ($productId -ne [Guid]::Empty) {
			$elApp = [System.Xml.XPath.Extensions]::XPathSelectElement($xDoc, "//ns:App", $nsManager)
			if ($elApp.Attribute("ProductID") -ne $null) {
				$elApp.SetAttributeValue("ProductID", $productId.ToString("B"))
			}
		}

		if (![string]::IsNullOrEmpty($paramTokenAzureBackendUrl)) {
			$elStartPage = [System.Xml.XPath.Extensions]::XPathSelectElement($xDoc, "//ns:App/ns:Properties/ns:StartPage", $nsManager)
            $paramTokenAzureBackendUrl = $paramTokenAzureBackendUrl.TrimEnd("/")
            $elStartPage.Value = $elStartPage.Value.Replace("~remoteAppUrl", $paramTokenAzureBackendUrl)

            $eventElements= @(
                "//ns:App/ns:Properties/ns:InstalledEventEndpoint",
                "//ns:App/ns:Properties/ns:UninstallingEventEndpoint"
                "//ns:App/ns:Properties/ns:UpgradedEventEndpoint"
            );

            $eventElements | foreach {
                $element = [System.Xml.XPath.Extensions]::XPathSelectElement($xDoc, $_, $nsManager)
                ProcessRemoteEventHandlerUrl $element
            }            
		}    
    
		if (![string]::IsNullOrEmpty($paramTokenClientID)) {
			$elRemoteWebApp = [System.Xml.XPath.Extensions]::XPathSelectElement($xDoc, "//ns:App/ns:AppPrincipal/ns:RemoteWebApplication", $nsManager)
			if ($elRemoteWebApp -ne $null) {
				$elRemoteWebApp.Attribute("ClientId").SetValue($paramTokenClientID)
			}
			else {
				Write-Warning "Cannot set ClientId of SharePoint-hosted app."
			}
		}

		ReplacePart $partSream $xDoc.ToString()
	}

	function ProcessElementsFile($zipPackage) {
		if ([string]::IsNullOrEmpty($paramTokenAzureBackendUrl)) {
			return
		}

		$part = GetPart $zipPackage '/elements*.xml'
        if ($part -eq $null) {
            return;
        }

		$partSream = $part.GetStream()

		$xDoc = [System.Xml.Linq.XDocument]::Load($partSream)
		$nsManager = New-Object System.Xml.XmlNamespaceManager(New-Object System.Xml.NameTable)
		$nsManager.AddNamespace("ns", "http://schemas.microsoft.com/sharepoint/")

		$clientWpContent = [System.Xml.XPath.Extensions]::XPathSelectElement($xDoc, "//ns:Elements/ns:ClientWebPart/ns:Content", $nsManager)
		$clientWpContentSrc = $clientWpContent.Attribute("Src")

        $paramTokenAzureBackendUrl = $paramTokenAzureBackendUrl.TrimEnd("/")
        $clientWpContentSrc.Value = $clientWpContentSrc.Value.Replace("~remoteAppUrl", $paramTokenAzureBackendUrl)

		ReplacePart $partSream $xDoc.ToString()
	}

	function ProcessFeatureFile($zipPackage) {
		$part = GetPart $zipPackage '/feature*.xml'
		$partSream = $part.GetStream()

		$xDoc = [System.Xml.Linq.XDocument]::Load($partSream)
		$nsManager = New-Object System.Xml.XmlNamespaceManager(New-Object System.Xml.NameTable)
		$nsManager.AddNamespace("ns", "http://schemas.microsoft.com/sharepoint/")

		$featureElement = [System.Xml.XPath.Extensions]::XPathSelectElement($xDoc, "//ns:Feature", $nsManager)
		$featureElement.Attribute("Title").SetValue($paramTokenAddinTitle)

		ReplacePart $partSream $xDoc.ToString()
	}

    function ProcessRemoteEventHandlerUrl($el) {
        if ($el -eq $null) {
            return
        }

        $uri = [System.Uri]$el.Value

        if ($uri -ne $null) {
            $paramTokenAzureBackendUrl = $paramTokenAzureBackendUrl.TrimStart('/')

            if ($uri.PathAndQuery -ne $null) {
                $relativeUrl = $uri.PathAndQuery.TrimStart('/')
                $el.Value = $paramTokenAzureBackendUrl + "/" + $relativeUrl
            } else {            
                $el.Value = $uri.OriginalString.Replace('~remoteAppUrl', $paramTokenAzureBackendUrl)
            }

	        Write-Host "Remote event handler URI: '$el.Value'"
        }
    }

	function GetPart($zipPackage, $partName) {
		return $zipPackage.GetParts() | Where-Object { $_.Uri.ToString().ToLower() -like $partName -and $_.Uri.ToString().ToLower() -NotLike "*.config.xml" }
	}

	function ReplacePart($partStream, $xml) {
        $partStream.Position = 0
        $partStream.SetLength(0)
        $writer = New-Object -TypeName System.IO.StreamWriter -ArgumentList $partStream
        $writer.Write($xml)
        $writer.Flush()
	}

	function CopyStream([IO.Stream] $source, [IO.Stream] $target) {
		$bufSize = 0x1000
		$buf = new-object byte[] $bufSize

		$bytesRead = 0;
		while (($bytesRead = $source.Read($buf, 0, $bufSize)) -gt 0) {
			$target.Write($buf, 0, $bytesRead);
		}
	}

	####################################################################################
	# Main Flow
	####################################################################################

	# prepare credentials to be used to connect to app catalog
	$securePassword = ConvertTo-SecureString $paramAppCatalogPassword -AsPlainText -Force 
	$creds = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($paramAppCatalogLogin, $securePassword)

	# init SP context
	$ctx = New-Object Microsoft.SharePoint.Client.ClientContext($paramAppCatalogUrl)
	$ctx.Credentials = $creds

	# load app catalog library and root folder
	$appsLibName = "Apps for SharePoint"
	$appsLib = $ctx.Web.Lists.GetByTitle($appsLibName)
	$ctx.Load($appsLib)
	$ctx.Load($appsLib.RootFolder)
	$ctx.ExecuteQuery()

	# handle files parameter
	if ($paramFile.LastIndexOf('/') -gt $paramFile.LastIndexOf('.')) {
		$paramFile = $paramFile.TrimEnd('/')
		$paramFile += '/*.app'
	}

	$appFile = Get-ChildItem -Path $paramFile -Recurse

	# handle .app files
	Write-Host "Processing '$appFile'..."

	$memoryStream = New-Object IO.MemoryStream
	$fileStream = New-Object IO.FileStream($appFile.FullName, [System.IO.FileMode]::Open)
	$fileStream.CopyTo($memoryStream);
	$fileStream.Dispose()

	$zipPackage = [System.IO.Packaging.Package]::Open($memoryStream, [System.IO.FileMode]::OpenOrCreate)

	ProcessAppManifest $zipPackage
	ProcessElementsFile $zipPackage
	#ProcessFeatureFile $zipPackage

	$memoryStream.Position = 0

	$fileName = $paramTokenAddinFileName
	if ([string]::IsNullOrEmpty($fileName)) {
		$fileName = $appFile.Name
	}

	Write-Host "Uploading modified app file '$fileName' to App Catalog..."

	# upload transformed .app file to App Catalog
	$fileURL = $appsLib.RootFolder.ServerRelativeUrl + "/" + $fileName

	$fileCreationInfo = New-Object Microsoft.SharePoint.Client.FileCreationInformation
	$fileCreationInfo.Overwrite = $true
	$fileCreationInfo.ContentStream = $memoryStream
	$fileCreationInfo.URL = $fileURL

	$addedFile = $appsLib.RootFolder.Files.Add($fileCreationInfo)
	$ctx.Load($addedFile)
	$ctx.ExecuteQuery()

	Write-Host "Finished uploading .app files to '$paramAppCatalogUrl' App Catalog."
} finally {
    Trace-VstsLeavingInvocation $MyInvocation
}
