# Overview
VSTS release/build step to deploy Provider-hosted SharePoint add-ins to SharePoint tenants.

This extension lets you deploy Provider-hosted add-ins to SharePoint tenants directly from your release definitions.

When deploying Provider-hosted add-ins to SharePoint you need to supply additional information to allow e.g. SharePoint tokens to be generated correctly.

These include
- Client ID
- URL for Provider backend

Additionally this extension supports the following configurations

- Configure the title of the add-in (as displayed in the app part)
- Configure name of the uploaded file (to allow multiple installations of the same add-in on the same tenant)

## Note:

> Usage of this extension requires credentials of a service account with *write permissions* to the SharePoint tenant app catalog

# Release Notes
## 1.2.25 (25-Jul-2018)
- Fixed issue when using the add-in for remote event handling
## 1.1.16 (20-Apr-2017)
- Added screenshots